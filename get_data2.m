function [chanListData,warnMsg]=get_data2(channels,dtype,start_time,duration,nds2site)
% GET_DATA2 - Get data from remote LIGO NDS2 server
% 
% chanListData=get_data2(channels,dtype,start_time,duration,nds2site)
%   channels:  cell array of channels 
%   (e.g. {'H1:LSC-DARM_ERR','H2:LSC-DARM_ERR'})
%              or string of just one channel 'H1:LSC-DARM_ERR'
%   dtype: frame('raw','minute','MTREND','second','STREND','L1_DMT_COO',..)
%   start_time: GPS second number (e.g. 855124987)
%   duration:  number of seconds (e.g. 5)
%   nds2site: [OPTIONAL] specify which nds2 server - 'cit','llo' or 'lho'
%
% chanListData is a structures with data in it
% e.g. aa = get_data('H1:LSC-DARM_ERR','raw',871234091,1e5)
%   .name - channel name
%   .data - time-series (double)
%   .rate - data rate (Hz)
%   .start - starting time (GPS seconds)
%   .duration - duration of time-series (seconds)
%   .exist - 0 if not found, 1 if found
%   .error - error on this channel
%
% -- to avoid printout of warning messages, add another output parameter
%   
%   [chanListData,warnMsg] = get_data2(...)
%
%   2017-06-01  Keith Thorne
%   2017-10-03  Keith Thorne. change to whole-list fetch, unless error
%   2020-05-08  Keith Thorne - add optional warnMsg output to stop 
%                              warning message preing
%   2021-01-06  Keith Thorne - correct handling of errors for single-channel queries
%   2021-07-02  Keith Thorne - remove GAP_HANDLER for raw data, add 'nds2site' input
%   2023-03-01  Brian Lantz - line 331, re-added channel name to successful queries
%   2023-11-03  Edgard Bonilla - added correct looping of channel names of
%                                successful and unsuccessful queries
%   2023-12-14  Keith Thorne - add NDS2_JAR_PATH for alternate nds2.jar
%                            location
%   2024-05-22  Keith Thorne - use external nds2AddJava() in package

% clear output
chanListData=[];
warnMsg=[];

% check input count
if (nargin < 4)
   fprintf('GET_DATA2 ERROR: you only have %d inputs, you need 4\n');
   return
end
% check output count
warnPrt = true;
if (nargout > 1)
   warnPrt = false;
end

% convert single-channel string to cell-array
if isstr(channels)
  channels = {channels};
end

% Add nds2 Java to java class path
[foundNds2]=nds2AddJava();
if(foundNds2 == false)
    fprintf('GET_DATA2 ERROR: nds2.jar not in Java classpath\n');
    return
end

% attempt to set site based on channels
% - all channels start with L - LLO
% - all channels start with H - LHO
% - anything else - CIT
[chanRow,chanCol] = size(channels);
numChan=0;
numL=0;
numH=0;
remoteSite=[];
for i=1:chanRow
   for j=1:chanCol
       thisChan=channels{i,j};
       numChan = numChan + 1;
       switch thisChan(1:1)
         case 'L'
           numL = numL + 1;
         case 'H'
           numH = numH + 1;
         otherwise
       end
    end
end
% skip out if not channels
if (numChan==0)
    fprintf('GET_DATA2 ERROR:  empty channel list\n,');
    return
end

% if NDS2 server parameter is set, use that for site
siteDef = false;
if (nargin > 4)
  if ~isempty(nds2site)
     siteDef = true;
  end 
end
if (siteDef)
    fprintf(' Using input def of NDS2 site - %s\n',nds2site);
    nds2site = lower(nds2site);
    if (strcmp(nds2site,'lho'))
      remoteSite = 'lho';
      remoteNds = 'nds.ligo-wa.caltech.edu';
      remoteIp = 31200;
    elseif (strcmp(nds2site,'llo'))
      remoteSite = 'llo';
      remoteNds = 'nds.ligo-la.caltech.edu';
      remoteIp = 31200;
    elseif (strcmp(nds2site,'cit'))
      remoteSite = 'cit';
      remoteNds = 'nds.ligo.caltech.edu';
      remoteIp = 31200;
    else
      fprintf(' nds2site unknown - use cit\n');
      remoteSite = 'cit';
      remoteNds = 'nds.ligo.caltech.edu';
      remoteIp = 31200;
    end
else
  if(numChan == numH)
    remoteSite = 'lho';
    remoteNds = 'nds.ligo-wa.caltech.edu';
    remoteIp = 31200;
  elseif (numChan == numL)
    remoteSite = 'llo';
    remoteNds = 'nds.ligo-la.caltech.edu';
    remoteIp = 31200;
  else
    remoteSite = 'cit';
    remoteNds = 'nds.ligo.caltech.edu';
    remoteIp = 31200;
  end
end
% truncate the start time to a GPS second
startTime = floor(abs(start_time));
% round duration up to nearest second
durTime = round(duration);
% calculate stop time
stopTime = startTime+durTime;

% If 'raw', add 'raw' type
% If 'second', add 's-trend' add .mean if no such trend definition
% If 'minute', add 'm-trend', add .mean if no such trend definition
%    also if minute trend, set times to divisible by 60
% If none of these (i.e. DMT frame), set warning
switch dtype
    case 'raw'
% loop over channels. If 'raw' not present, add it.
        dataType = '';    
        defSuffix = []; 
    case 'second'
        dataType = 's-trend';
        defSuffix = 'mean';
    case 'STREND'
        dataType = 's-trend';
        defSuffix = 'mean';
    case 'strend'
        dataType = 's-trend';
        defSuffix = 'mean';
    case 'minute'
        dataType = 'm-trend';
        defSuffix = 'mean';
% For minute trends, it has to be multiple of 60
        startTime = 60*round(startTime/60);
        stopTime = 60*round(stopTime/60);
        durTime = stopTime - startTime;        
    case 'MTREND'
        dataType = 'm-trend';
        defSuffix = 'mean';
% For minute trends, it has to be multiple of 60
        startTime = 60*round(startTime/60);
        stopTime = 60*round(stopTime/60);
        durTime = stopTime - startTime;        
    case 'mtrend'
        dataType = 'm-trend';
        defSuffix = 'mean';
% For minute trends, it has to be multiple of 60
        startTime = 60*round(startTime/60);
        stopTime = 60*round(stopTime/60);
        durTime = stopTime - startTime;        
    case []
        dataType = 'raw';    
        defSuffix = [];         
    otherwise
        dataType = dtype;
        defSuffix = [];
        warnMsg = sprintf('WARNING: frame type of %s not raw, second or minute\n',dtype);
        if warnPrt
            fprintf('GET_DATA2 %s\n',warnMsg);
        end    
end
% note if changing time range
if ((startTime ~= start_time) || (durTime ~= duration))
    warnMsg = sprintf('WARNING: start time, duration corrected to %d and %d',startTime,durTime);
    if warnPrt
       fprintf('GET_DATA2 %s\n',warnMsg);
    end    
end
%
% remember which NDS1/2 server we are using.
% - We have to have a different persistent object for each possible one
% because java object don't play nice
persistent activeSite;
persistent lloConn;
persistent lhoConn;
persistent citConn;

if ~strcmp(remoteSite,activeSite)
    activeSite = remoteSite;
    switch activeSite
      case 'lho'
        if isempty(lhoConn)    
          try
            lhoConn=nds2.connection(remoteNds,remoteIp);
          catch ME_nds
            fprintf('GET_DATA2 ERROR: nds connection to %s %d failed %s\n',remoteNds,remoteIp,ME_nds.message);
            return
          end
    % only patch gaps for minute trends
          if strcmp(dataType,'m-trend')
             lhoConn.setParameter('GAP_HANDLER','STATIC_HANDLER_ZERO');
          else
             lhoConn.setParameter('GAP_HANDLER','ABORT_HANDLER');
          end
    % We also always wait for the data
    	  lhoConn.setParameter('ALLOW_DATA_ON_TAPE','1');
          fprintf('GET_DATA2: nds connection to %s %d succeeded\n',remoteNds,remoteIp);
        end  
      case 'llo'
        if isempty(lloConn)    
          try
            lloConn=nds2.connection(remoteNds,remoteIp);
          catch ME_nds
            fprintf('GET_DATA2 ERROR: nds connection to %s %d failed %s\n',remoteNds,remoteIp,ME_nds.message);
            return
          end
    % only patch gaps for minute trends
          if strcmp(dataType,'m-trend')
             lloConn.setParameter('GAP_HANDLER','STATIC_HANDLER_ZERO');
          else
             lloConn.setParameter('GAP_HANDLER','ABORT_HANDLER');
          end
    % We also always wait for the data
    	  lloConn.setParameter('ALLOW_DATA_ON_TAPE','1');
          fprintf('GET_DATA2: nds connection to %s %d succeeded\n',remoteNds,remoteIp);
        end  
      case 'cit'
        if isempty(citConn)    
          try
            citConn=nds2.connection(remoteNds,remoteIp);
          catch ME_nds
            fprintf('GET_DATA2 ERROR: nds connection to %s %d failed %s\n',remoteNds,remoteIp,ME_nds.message);
            return
          end
    % only patch gaps for minute trends
          if strcmp(dataType,'m-trend')
             citConn.setParameter('GAP_HANDLER','STATIC_HANDLER_ZERO');
          else
             citConn.setParameter('GAP_HANDLER','ABORT_HANDLER');
          end
    % We also always wait for the data
    	  citConn.setParameter('ALLOW_DATA_ON_TAPE','1');
          fprintf('GET_DATA2: nds connection to %s %d succeeded\n',remoteNds,remoteIp);
        end
      otherwise
          fprintf('GET_DATA2 ERROR: unknown NDS site %s \n',activeSite);
    end  
end

% Loop over channels, getting data for each
[chanRow,chanCol] = size(channels);
%  first we try to do full list in one go.  If there is an error, we 
%  repeat, doing one-channel at a time to get full errors
%  Loop over channels
%    Build up channel list
%  end Loop
%  Do full-list fetch
%  If OK
%     Loop over channels
%        Build output
%     end Loop
%  else (not OK)
%     Loop over channels
%       Build channel desc
%       fetch data for channel normal case - loop over channels to build list, call fetch, 
%       Build up output
%     end loop
%  end
idx=0;
chanGlob=cell(1);
numChan = 0;
for i=1:chanRow
  for j=1:chanCol
      idx = idx + 1;
      numChan = idx;
      thisChan = channels{i,j};
      [chanName,chanSuf] = strtok(thisChan,'.');
      if (~isempty(defSuffix))
          if (isempty(chanSuf))
              chanSuf = strcat('.',defSuffix);
          end
      end
      if (~isempty(dataType))
         thisChan = strcat(chanName,chanSuf,',',dataType);
      else
         thisChan = strcat(chanName,chanSuf);
      end
% build channel list for 'fetch'        
      chanGlob{1,idx} = thisChan; 
  end
end
% now make the call
try
    switch activeSite
        case 'lho'
            dataBuff = lhoConn.fetch(startTime,stopTime,chanGlob);
        case 'llo'
            dataBuff = lloConn.fetch(startTime,stopTime,chanGlob);
        case 'cit'
            dataBuff = citConn.fetch(startTime,stopTime,chanGlob);
    end
    fetchOk = true;
catch ME_fetch
    fetchOk = false;
    rtIdx = strfind(ME_fetch.message,'RuntimeException');
    if(rtIdx > 0)
        nfIdx = strfind(ME_fetch.message, 'Requested data were not found');
        if (nfIdx > 0)
            errMsg = 'Channel not found';
        else
            errMsg = 'RunTimeException';
        end
    else
        errMsg = ME_fetch.message;
    end
    fprintf('GET_DATA2 ERROR: nds2 failed on fetch  %s\n',ME_fetch.message);  
end
% If OK, now tediously loop over channels again to pull out data
if ( fetchOk ) 
  idx=0;
  for i=1:chanRow
    for j=1:chanCol
        idx=idx+1;
        chanData = dataBuff(idx).getData();
        thisChan = chanGlob{1,idx};
        if ~isempty(chanData)
            chanListData(idx).exist = 1;
            chanListData(idx).error = [];
        else
            chanListData(idx).exist = 0;
            chanListData(idx).error = errMsg;
        end 
        chanListData(idx).name = thisChan;
        chanListData(idx).data = double(chanData);            
        chanRate = dataBuff(idx).getChannel().getSampleRate();
        chanListData(idx).rate = chanRate;
        chanListData(idx).start = startTime;
        chanListData(idx).duration = durTime;
    end
  end
% if not OK but only one channel we are done
elseif (numChan == 1)
  thisChan = chanGlob{1,1};
  chanListData(1).name = thisChan;
  chanListData(1).data = [];
  chanListData(1).rate = [];
  chanListData(1).start = startTime;
  chanListData(1).duration = durTime;
  chanListData(1).exist = 0;
  chanListData(1).error = errMsg;
% otherwise, re-do per channel to get specific errors
else
  fprintf('GET_DATA2: Redo fetch to get per-channel errors\n');   
  idx=0;  
  for i=1:chanRow
    for j=1:chanCol
        idx = idx + 1;
        thisChan = chanGlob{1,idx};
        try
            switch activeSite
                case 'lho'
                    dataBuff = lhoConn.fetch(startTime,stopTime,thisChan);
                case 'llo'
                    dataBuff = lloConn.fetch(startTime,stopTime,thisChan);
                case 'cit'
                    dataBuff = citConn.fetch(startTime,stopTime,thisChan);
            end
            chanData = dataBuff(1).getData();
            chanRate = dataBuff(1).getChannel().getSampleRate();
        catch ME_chan
            rtIdx = strfind(ME_chan.message,'RuntimeException');
            if(rtIdx > 0)
                nfIdx = strfind(ME_chan.message, 'Requested data were not found');
                if (nfIdx > 0)
                    errMsg = 'Channel not found';
                else
                    errMsg = 'RunTimeException';
                end
            else
                errMsg = ME_chan.message;
            end
            fprintf('GET_DATA2 ERROR: nds2 failed on %s - %s\n',thisChan,ME_chan.message);  
            chanData = [];
            chanRate = [];
        end   
        chanListData(idx).name = thisChan;
        chanListData(idx).data = double(chanData);
        chanListData(idx).rate = chanRate;
        chanListData(idx).start = startTime;
        chanListData(idx).duration = durTime;
        if ~isempty(chanData)
            chanListData(idx).exist = 1;
            chanListData(idx).error = [];
        else
            chanListData(idx).exist = 0;
            chanListData(idx).error = errMsg;
        end   
    end
  end
end

return
 
