function [varargout] = checkout_get_data(varargin)
% ([errCode]) = CHECKOUT_GET_DATA - verify that get_data install is working
%
%    errCode = [OPTIONAL] error code - 0 if pass

% Clear errors 
errCode = 0;
% Make sure get_data is in the MATLAB path
get_dataPath = which('get_data');
if isempty(get_dataPath)
    fprintf('checkout_get_data: get_data.m is not in MATLABPATH - FAIL\n');
    errCode = errCode + 1;
    if (nargout > 0)
        varargout{1}=errCode;
    end
    return    
end
fprintf('checkout_get_data: get_data.m is in MATLABPATH - PASS\n');
% Make sure LIGONDSIP is defined 
localNds=getenv('LIGONDSIP');
if isempty(localNds)
    fprintf('checkout_get_data: LIGONDSIP not defined - FAIL\n');
    errCode = errCode + 2;    
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_get_data: LIGONDSIP value of %s - PASS\n',localNds);
% Make sure LIGONDSIP is reachable
pingCmd = sprintf('ping -c 3 %s',localNds);
[pingStat,pingResp]=unix(pingCmd);
if pingStat > 0
    fprintf('checkout_get_data: NDS server %s is not pingable - FAIL\n',localNds);
    errCode=errCode + 4;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_get_data: NDS server is pingable - PASS\n')
% Make sure NDS server is running
telnetCmd=sprintf('sleep 2 | telnet %s 8087',localNds);
[telnetStat,telnetResp]=unix(telnetCmd);
if ~contains(telnetResp,'daqd')
    fprintf('checkout_get_data: daqd not running on %s - FAIL\n',localNds);
    errCode=errCode+8
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_get_data: daqd is running on %s - PASS\n',localNds);
% Make sure nds2 class in in the java path
dbgPrint = true;
[foundNds,ndsPath]=nds2AddJava(dbgPrint);
if ~foundNds
    fprintf('checkout_get_data: Failed to find nds2 class - FAIL\n');
    errCode = errCode + 16;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_get_data: nds2 in Java class path - PASS\n');
% try to make connections
localIp=8088;
try
    localConn=nds2.connection(localNds,localIp);
catch ME_nds
    fprintf('checkout_get_data: nds2 connection to %s %d failed %s - FAIL\n',localNds,localIp,ME_nds.message);
    errCode = errCode + 32;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_get_data: nds2 connection to %s %d made - PASS\n',localNds,localIp);
% If successful, get a count
try
    numChan=localConn.countChannels();
catch ME_nds
    fprintf('checkout_get_data: unable to get channel count from NDS server %s - FAIL\n',ME_nds.message);
    errCode = errCode + 64;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_get_data: found %d channels on NDS server - PASS\n',numChan);
% get a small set of channels and try to retrieve recent data
% create channel list with faster channels
try
    fastList=localConn.findChannels('*',nds2.channel.CHANNEL_TYPE_ONLINE,nds2.channel.DATA_TYPE_FLOAT32, 1024, 16384); 
catch ME_nds
    fprintf('checkout_get_data: Unable to retrieve list of fast channels from NDS server %s - FAIL\n',ME_nds.message);
    errCode = errCode + 128;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_get_data: Got list of fast channels over 1 KHz from NDS server - PASS\n');
% make sure we have some
numFast=length(fastList);
if (numFast < 3)     
    fprintf('checkout_get_data: Only %d fast channels being recorded on NDS server - FAIL\n',numFast);
    errCode = errCode + 256;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_get_data: There are %d channels over 1 KHz from NDS server - PASS\n',numFast);
% close this connection
localConn.close();
% Make sure gpstime is available
gpsCmd='gpstime -g';
[gpsStat,gpsResp]=unix(gpsCmd);
if gpsStat > 0
    fprintf('checkout_get_data: gpstime not in path - FAIL\n');
    errCode = errCode + 512;
    if (nargout > 0)
        varargout{1}=errCode;    
    end
    return    
end
fprintf('checkout_get_data: gpstime utility installed - PASS\n');
% truncate GPS time and set to 15 minutes in the past
gpsNow=round(str2num(gpsResp));
gpsStart = gpsNow - 15*60;
% Create channel list
chan1=char(fastList(1).name);
chan2=char(fastList(2).name);
chan3=char(fastList(3).name);
chanNames={chan1,chan2,chan3};
% call get_data
[chanListData,warnMsg]=get_data(chanNames,'raw',gpsStart,20);
% make sure all is good
getOk = true;
for i=1:3
    if (chanListData(i).exist == 0)
        fprintf('checkout_get_data: Channel %i does not exist on NDS server \n',i);
        getOk = false;
    end
    if (~isempty(chanListData(i).error))
        fprintf('checkout_get_data: Channel %i has error %s\n',i,chanListData(i).error);
        getOk = false;
    end
end
if getOk == false
    errCode = errCode + 1024;
    if (nargout > 0)
        varargout{1}=errCode;    
        fprintf('checkout_get_data: Unable to retrieve channel data with get_data - FAIL\n');
    end
    return
end
fprintf('checkout_get_data: Success in retrieving channel data with get_data - PASS\n');
if(nargout > 0)
    varargout{1} = 0;
end
