function [varargout] = checkout_get_data2(varargin)
%([errCode]) = CHECKOUT_GET_DATA2([nds2_site]) - verify that get_data2 install is working
%  
%    nds2_site = [OPTIONAL] specify NDS2 server site - 'llo','lho','cit'
%                default is llo
%    errCode = [OPTIONAL] error code - 0 if pass

% Make sure the NDS2 server is there
% Set up connection to NDS2 server
% get channel count
% get recent list of fast channels
% retrieve channel data from NDS2 server
%
errCode = 0;
ndsSite = 'llo';
remoteNds = 'nds.ligo-la.caltech.edu';
ifoPre = 'L1:*';
if(nargin > 0)
    testSite = varargin{1};
    switch testSite
        case 'llo'
            ndsSite = 'llo';
            remoteNds = 'nds.ligo-la.caltech.edu';
            ifoPre = 'L1:*';
        case 'lho'
            ndsSite = 'lho';
            remoteNds = 'nds.ligo-wa.caltech.edu';
            ifoPre = 'H1:*';
        case 'cit'
            ndsSite = 'cit';
            remoteNds = 'nds.ligo.caltech.edu';
            ifoPre = 'L1:*';
        otherwise
            fprintf('checkout_get_data2: testSite %s does not match - default to llo\n');
    end
end
% Make sure get_data2 is in the MATLAB path
get_dataPath = which('get_data2');
if isempty(get_dataPath)
    fprintf('checkout_get_data2: get_data2.m is not in MATLABPATH - FAIL\n');
    errCode = errCode + 1;
    if (nargout > 0)
        varargout{1}=errCode;
    end
    return    
end
fprintf('checkout_get_data2: get_data2.m is in MATLABPATH - PASS\n');
% Make sure NDS2 server is reachable
pingCmd = sprintf('ping -c 3 %s',remoteNds);
[pingStat,pingResp]=unix(pingCmd);
if pingStat > 0
    fprintf('checkout_get_data2: NDS2 server %s is not pingable - FAIL\n',remoteNds);
    errCode=errCode + 2;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_get_data2: NDS2 server %s is pingable - PASS\n',remoteNds)
% Make sure nds2 class in in the java path
dbgPrint = true;
[foundNds,ndsPath]=nds2AddJava(dbgPrint);
if ~foundNds
    fprintf('checkout_get_data2: Failed to find nds2 class - FAIL\n');
    errCode = errCode + 4;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_get_data2: nds2 in Java class path - PASS\n');
% check for Kerberos ticket
klistCmd = 'klist -s';
[klistStat]=unix(klistCmd);
if klistStat > 0
    fprintf('checkout_get_data2: No valid Kerberos ticket - FAIL if not on site workstation\n');
else
    fprintf('checkout_get_data2: Valid Kerberos ticket - PASS\n');    
end
% try to make connections
remoteIp=31200;
try
    remoteConn=nds2.connection(remoteNds,remoteIp);
catch ME_nds
    fprintf('checkout_get_data2: nds2 connection to %s %d failed %s - FAIL\n',remoteNds,remoteIp,ME_nds.message);
    errCode = errCode + 8;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_get_data2: nds2 connection to %s %d made - PASS\n',remoteNds,remoteIp);
% If successful, get a count of IFO channels
chanType = nds2.channel.CHANNEL_TYPE_RAW;
try   
    numChan=remoteConn.countChannels(ifoPre,chanType);
catch ME_nds
    fprintf('checkout_get_data2: unable to get IFO channel count from NDS2 server %s - FAIL\n',ME_nds.message);
    errCode = errCode + 16;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_get_data2: found %d IFO channels on NDS2 server - PASS\n',numChan);
% get a small set of channels and try to retrieve recent data
% - To limit to recent channels, we first need GPS time
% Make sure gpstime is available
gpsCmd='gpstime -g';
[gpsStat,gpsResp]=unix(gpsCmd);
if gpsStat > 0
    fprintf('checkout_get_data2: gpstime not in path - FAIL\n');
    errCode = errCode + 32;
    if (nargout > 0)
        varargout{1}=errCode;    
    end
    return    
end
fprintf('checkout_get_data2: gpstime utility installed - PASS\n');
% truncate GPS time and set to 4 hours in the past
gpsNow=round(str2num(gpsResp));
gpsStart = gpsNow - 4*60*60;
% Define GPS epoch
gpsEnd = gpsStart + 3600;
try
    remoteConn.setEpoch(gpsStart,gpsEnd);
catch ME_nds
    fprintf('checkout_get_data2: unable to get GPS epoch on NDS2 server %s - FAIL\n',ME_nds.message);
    errCode = errCode + 64;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end    
% create channel list with faster channels
try
    fastList=remoteConn.findChannels(ifoPre,chanType,nds2.channel.DATA_TYPE_FLOAT32, 1024, 16384); 
catch ME_nds
    fprintf('checkout_get_data2: Unable to retrieve list of fast channels from NDS2 server %s - FAIL\n',ME_nds.message);
    errCode = errCode + 128;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_get_data2: Got list of recent fast channels over 1 KHz from NDS2 server - PASS\n');
% make sure we have some
numFast=length(fastList);
if (numFast < 3)     
    fprintf('checkout_get_data2: Only %d IFO fast channels on NDS2 server - FAIL\n',numFast);
    errCode = errCode + 256;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_get_data2: There are %d recent IFO channels over 1 KHz online at NDS2 server - PASS\n',numFast);
% close this connection
remoteConn.close();
% Create channel list
chan1=char(fastList(1).name);
chan2=char(fastList(2).name);
chan3=char(fastList(3).name);
chanNames={chan1,chan2,chan3};
% call get_data
[chanListData,warnMsg]=get_data2(chanNames,'raw',gpsStart,20,ndsSite);
% make sure all is good
getOk = true;
for i=1:3
    if (chanListData(i).exist == 0)
        fprintf('checkout_get_data2: Channel %i does not exist on NDS2 server \n',i);
        getOk = false;
    end
    if (~isempty(chanListData(i).error))
        fprintf('checkout_get_data2: Channel %i has error %s\n',i,chanListData(i).error);
        getOk = false;
    end
end
if getOk == false
    errCode = errCode + 512;
    if (nargout > 0)
        varargout{1}=errCode;    
        fprintf('checkout_get_data2: Unable to retrieve channel data with get_data2 - FAIL\n');
    end
    return
end
fprintf('checkout_get_data2: Success in retrieving channel data with get_data2 - PASS\n');
if(nargout > 0)
    varargout{1} = 0;
end
